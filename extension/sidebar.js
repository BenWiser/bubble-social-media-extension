const imageUrl = (image) => chrome.runtime.getURL(image);

const createSidebar = () => {
    const sideBar = document.createElement('div');
    sideBar.className = "bubbleSocialMediaClass";
    sideBar.innerHTML = `
        <div class="bubbleNavBar">
            <a id="showFeedButton">
                <img src=${imageUrl('images/globe.png')} />
                <small>Feed</small>
            </a>
            <a id="showCommentsButton">
                <img src=${imageUrl('images/comment.png')} />
                <small>Comments</small>
            </a>
            <a id="showFollowersButton">
                <img src=${imageUrl('images/search.png')} />
                <small>Followers</small>
            </a>
        </div>
        <div class="bubbleFeedPage">
        </div>
        <div class="bubbleCommentPage">
            <form id="bubbleCommentForm">
                <textarea id="bubbleAddComment"></textarea>
                <input type="submit" value="Comment" />
            </form>
            <hr />
            <div class="bubbleCommentStream">
            </div>
        </div>
        <div class="bubbleRequestPage">
            <form id="bubbleNameForm" class="nameForm">
                <input placeholder="Name" type="text" name="name" />
                <input type="submit" value="Change name" />
            </form>
            <hr />
            <div class="bubbleSubNav">
                <a id="bubbleFollowSection">
                    <img src=${imageUrl('images/find.png')} />
                    <small>Follow</small>
                    <input type="file" accept=".identity" hidden />
                </a>
                <a class="bubbleIdentitySection">
                    <img src=${imageUrl('images/share.png')} />
                    <small>Share identity</small>
                </a>
            </div>
            <hr />
            <div class="followerRequests">
                <h1>Followers</h1>
                <ul>
                </ul>
            </div>
        </div>
    `;
    sideBar.style.display = "none";
    return sideBar;
};

const sideBar = createSidebar();
document.body.appendChild(sideBar);

function changeName(event) {
    event.preventDefault();
    const name = event.target.children[0].value;
    const changeNameEvent = new CustomEvent('ChangeNameEvent', {
        detail: {
            name,
        },
    });
    document.dispatchEvent(changeNameEvent);
}

function comment(event) {
    event.preventDefault();
    const comment = event.target.children[0].value;
    const commentEvent = new CustomEvent('CommentEvent', {
        detail: {
            comment,
        },
    });
    document.dispatchEvent(commentEvent);
    event.target.children[0].value = "";
}

function hideAllPages() {
    const feedPage = document.querySelector('.bubbleFeedPage');
    const commentPage = document.querySelector('.bubbleCommentPage');
    const requestPage = document.querySelector('.bubbleRequestPage');

    feedPage.style.display = "none";
    commentPage.style.display = "none";
    requestPage.style.display = "none";
}

function showPage(pageSelector) {
    hideAllPages();
    const page = document.querySelector(pageSelector);
    page.style.display = "";
}

showPage('.bubbleCommentPage');

const setBubbleIdentityFileDownload = async () => {
    const [userId, authenticationKey, keyPair, name] = await Promise.all([
        State.userId,
        State.authenticationKey,
        State.keyPair,
        State.name,
    ]);

    const downloadButton = sideBar.querySelector('.bubbleIdentitySection');
    downloadButton.setAttribute('href', `data:text/plain;charset=utf-8,${encodeURIComponent(userId + "\n" + authenticationKey + "\n" + keyPair.publicKey)}`);
    downloadButton.setAttribute('download', `${name} Bubble.identity`);
};

const toggleSidebar = () => {
    const isVisible = sideBar.style.display !== "none";
    sideBar.style.display = isVisible ? "none" : "";
    return !isVisible;
};

const updateComments = async () => {
    const comments = await State.comments;
    const bubbleCommentStream = document.querySelector('.bubbleCommentStream');
    const feedPage = document.querySelector('.bubbleFeedPage');

    bubbleCommentStream.innerHTML = comments
    .filter(({ url }) => location.href === url)
    .reverse()
    .map(({ name, message }) => (`
        <div>
        ${name}: ${message}
        </div>
    `)).join('');

    feedPage.innerHTML = Object.entries(groupBy(comments, 'url'))
        .reverse()
        .map(([key, value]) => (`
            <a href="${key}">
                <div>
                    <strong>${value[0].pageTitle}</strong>
                    <br />Visited by ${Array.from(new Set(value.map(({ name }) => name))).join(', ')}
                </div>
            </a>
        `)).join('');
};

chrome.runtime.onMessage.addListener(async (request, sender, sendResponse) => {
    switch (request.action) {
        case "open_sidebar": {
            const isVisible = toggleSidebar();
            if (isVisible) {
                sync();
            }
            break;
        }
    }
});

// Set the initial name
(async () => {
    const name = await State.name;
    sideBar.querySelector('.nameForm input[type=text]').value = name;
})();

document.getElementById("showFeedButton").addEventListener("click", () => {
    showPage('.bubbleFeedPage');
});

document.getElementById("showCommentsButton").addEventListener("click", () => {
    showPage('.bubbleCommentPage');
});

document.getElementById("showFollowersButton").addEventListener("click", () => {
    showPage('.bubbleRequestPage');
});

document.getElementById("bubbleCommentForm").addEventListener("submit", (event) => {
    comment(event);
});

document.getElementById("bubbleNameForm").addEventListener("submit", (event) => {
    changeName(event);
});

document.getElementById("bubbleFollowSection").addEventListener("click", () => {
    const file = document.querySelector('#bubbleFollowSection input[type=file]');
    file.onchange = () => {
        const reader = new FileReader();
        reader.onload = ({ target: { result } }) => {
            const followEvent = new CustomEvent('FollowEvent', {
                detail: {
                    bubbleIdentity: result,
                },
            });
            document.dispatchEvent(followEvent);
        };

        reader.readAsBinaryString(file.files[0]);
    };
    file.click();
});