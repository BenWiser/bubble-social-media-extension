const State = {
    get userId() {
        return new Promise((resolve) =>
            chrome.storage.local.get(['userId'], ({ userId }) => {
                resolve(userId);
            })
        );
    },

    set userId(userId) {
        chrome.storage.local.set({
            userId: userId,
        });
    },

    get followers() {
        return new Promise((resolve) =>
            chrome.storage.local.get(['followers'], ({ followers }) => {
                resolve(followers ? followers : []);
            })
        );
    },

    set followers(followers) {
        chrome.storage.local.set({
            followers: followers,
        });
    },

    get name() {
        return new Promise((resolve) =>
            chrome.storage.local.get(['name'], ({ name }) => {
                resolve(name ? name : "");
            })
        );
    },

    set name(name) {
        chrome.storage.local.set({
            name: name,
        });
    },

    get keyPair() {
        return new Promise((resolve) =>
            chrome.storage.local.get(['keyPair'], ({ keyPair }) => {
                resolve(keyPair);
            })
        );
    },

    set keyPair(keyPair) {
        chrome.storage.local.set({
            keyPair: keyPair,
        });
    },

    get authenticationKey() {
        return new Promise((resolve) =>
            chrome.storage.local.get(['authenticationKey'], ({ authenticationKey }) => {
                resolve(authenticationKey);
            })
        );
    },

    set authenticationKey(authenticationKey) {
        chrome.storage.local.set({
            authenticationKey: authenticationKey,
        });
    },

    get lastChecked() {
        return new Promise((resolve) =>
            chrome.storage.local.get(['lastChecked'], ({ lastChecked }) => {
                resolve(lastChecked);
            })
        );
    },

    set lastChecked(lastChecked) {
        chrome.storage.local.set({
            lastChecked: lastChecked,
        });
    },

    get comments() {
        return new Promise((resolve) =>
            chrome.storage.local.get(['comments'], ({ comments }) => {
                resolve(comments);
            })
        );
    },

    set comments(comments) {
        chrome.storage.local.set({
            comments: comments,
        });
    },

    async addComment(comment) {
        const comments = await this.comments;
        chrome.storage.local.set({
            comments: comments ? [...comments, comment] : [comment],
        });
    },

    get authenticationIds() {
        return new Promise((resolve) =>
            chrome.storage.local.get(['authenticationIds'], ({ authenticationIds }) => {
                resolve(authenticationIds ? authenticationIds : []);
            })
        );
    },

    async addAuthenticationId(authenticationId) {
        const authenticationIds = await this.authenticationIds;
        chrome.storage.local.set({
            authenticationIds: authenticationIds.concat(authenticationId),
        });
    }
};