const ENDPOINT = 'https://benwiser.com/bubble';

const requestDefault = (path, method = 'GET', body = undefined) => {
    return fetch(`${ENDPOINT}/${path}`, {
        method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(body)
    });
};

const requestText = (path, method = 'GET', body = undefined) => {
    return requestDefault(path, method, body)
    .then(response => response.text());
};

const requestJson = (path, method = 'GET', body = undefined) => {
    return requestDefault(path, method, body)
    .then(response => response.json());
};