const Crypto = {
    ENCRYPTION_ALGORITHM: "RSA-OAEP",
    SYMETRIC_ENCRYPTION_ALGORITHM: "AES-GCM",
    AUTHENTICATION_ALGORITHM: "HMAC",
    AUTHENTICATION_HASHING_ALGORITHM: "SHA-512",
    TEXT_ENCODER: new TextEncoder(),
    TEXT_DECODER: new TextDecoder(),

    async createKeyPar() {
        const keyPair = await crypto.subtle.generateKey(
            {
                name: Crypto.ENCRYPTION_ALGORITHM,
                modulusLength: 4096,
                publicExponent: new Uint8Array([1, 0, 1]),
                hash: "SHA-256"
            },
            true,
            ["encrypt", "decrypt"],
        );
        return keyPair;
    },

    async createAuthenticationKey() {
        const authenticationKey = await crypto.subtle.generateKey(
            {
                name: Crypto.AUTHENTICATION_ALGORITHM,
                hash: {
                    name: Crypto.AUTHENTICATION_HASHING_ALGORITHM,
                },
            },
            true,
            ["sign", "verify"],
        );
        return authenticationKey;
    },

    async createSymetricKey() {
        const symetricKey = await crypto.subtle.generateKey(
            {
              name: Crypto.SYMETRIC_ENCRYPTION_ALGORITHM,
              length: 256
            },
            true,
            ["encrypt", "decrypt"]
        );
        return symetricKey;
    },

    async exportAuthenticationKey(key) {
        const exportedKey = await crypto.subtle.exportKey("raw", key);
        return new Uint8Array(exportedKey);
    },

    async exportSymetricKey(key, iv) {
        const exportedPrivateKey = await crypto.subtle.exportKey("raw", key);
        return {
            key: Crypto.convertArrayToBase64(new Uint8Array(exportedPrivateKey)),
            iv: Crypto.convertArrayToBase64(iv),
        };
    },

    async exportPrivateKey(keyPair) {
        const exportedPrivateKey = await crypto.subtle.exportKey("pkcs8", keyPair.privateKey);
        return new Uint8Array(exportedPrivateKey);
    },

    async exportPublicKey(keyPair) {
        const exportPublicKey = await crypto.subtle.exportKey("spki", keyPair.publicKey);
        return new Uint8Array(exportPublicKey);
    },

    async importSymetricKey(exportedSymetricKey) {
        const arrayKey = Crypto.convertBase64toArray(exportedSymetricKey.key);
        const iv = Crypto.convertBase64toArray(exportedSymetricKey.iv);
        const importedAuthenticationKey = await crypto.subtle.importKey(
            "raw",
            arrayKey,
            {
                name: Crypto.SYMETRIC_ENCRYPTION_ALGORITHM,
                length: 256
            },
            false,
            ["encrypt", "decrypt"],
        );
        return {
            iv,
            key: importedAuthenticationKey,
        };
    },

    async importAuthenticationKey(authenticationKeyBuffer) {
        const importedAuthenticationKey = await crypto.subtle.importKey(
            "raw",
            authenticationKeyBuffer,
            {
                name: Crypto.AUTHENTICATION_ALGORITHM,
                hash: {
                    name: Crypto.AUTHENTICATION_HASHING_ALGORITHM,
                },
            },
            false,
            ["sign", "verify"],
        );
        return importedAuthenticationKey;
    },

    async importPrivateKey(privateKeyBuffer) {
        const importedPrivateKey = await crypto.subtle.importKey(
            "pkcs8",
            privateKeyBuffer,
            {
                name: Crypto.ENCRYPTION_ALGORITHM,
                modulusLength: 4096,
                publicExponent: new Uint8Array([1, 0, 1]),
                hash: "SHA-256",
            },
            false,
            ["decrypt"],
        );
        return importedPrivateKey;
    },

    async importPublicKey(publicKeyBuffer) {
        const importedPublicKey = await crypto.subtle.importKey(
            "spki",
            publicKeyBuffer,
            {
                name: Crypto.ENCRYPTION_ALGORITHM,
                modulusLength: 4096,
                publicExponent: new Uint8Array([1, 0, 1]),
                hash: "SHA-256",
            },
            false,
            ["encrypt"],
        );
        return importedPublicKey;
    },

    async sign(key, message) {
        const encodedText = Crypto.TEXT_ENCODER.encode(message);
        const signed = await crypto.subtle.sign(
            Crypto.AUTHENTICATION_ALGORITHM,
            key,
            encodedText,
        );
        return signed;
    },

    async verify(key, signed, message) {
        const encodedText = Crypto.TEXT_ENCODER.encode(message);
        const isValid = await crypto.subtle.verify(
            Crypto.AUTHENTICATION_ALGORITHM,
            key,
            signed,
            encodedText,
        );
        return isValid;
    },

    async encryptMessage(publicKey, message, algorithm = Crypto.ENCRYPTION_ALGORITHM, extraKeys = {}) {
        const encodedText = Crypto.TEXT_ENCODER.encode(message);
        const encryptedMessage = await crypto.subtle.encrypt({
            name: algorithm,
            ...extraKeys,
        }, publicKey, encodedText);
        return encryptedMessage;
    },

    async encryptSymetricMessage(key, message, iv) {
        return Crypto.encryptMessage(key, message, Crypto.SYMETRIC_ENCRYPTION_ALGORITHM, {
            iv,
        });
    },

    async decryptMessage(privateKey, encryptedMessage, algorithm = Crypto.ENCRYPTION_ALGORITHM, extraKeys = {}) {
        return Crypto.TEXT_DECODER.decode(await window.crypto.subtle.decrypt({
            name: algorithm,
            ...extraKeys,
        }, privateKey, encryptedMessage));
    },

    async decryptSymetricMessage(key, message, iv) {
        return Crypto.decryptMessage(key, message, Crypto.SYMETRIC_ENCRYPTION_ALGORITHM, {
            iv,
        });
    },

    convertArrayToBase64(exportedKey) {
        return btoa(String.fromCharCode.apply(null, exportedKey));
    },

    convertBase64toArray(base64) {
        return new Uint8Array(atob(base64).split("").map((char) => char.charCodeAt(0)));
    }
};

const initialiseAuthenticationKey = (async () => {
    const authenticationKey = await State.authenticationKey;
    if (authenticationKey !== undefined) {
        return authenticationKey;
    }

    const newAuthenticationKey = await Crypto.createAuthenticationKey();
    const exported = await Crypto.exportAuthenticationKey(newAuthenticationKey);
    const base64AuthenticationKey = Crypto.convertArrayToBase64(exported);
    State.authenticationKey = base64AuthenticationKey;
    return base64AuthenticationKey;
})();

const initialiseKeyPair = (async () => {
    const keyPair = await State.keyPair;
    if (keyPair !== undefined) {
        return keyPair;
    }

    const cryptoKeyPair = await Crypto.createKeyPar();
    const [exportedPublic, exportedPrivate] = await Promise.all([
        Crypto.exportPublicKey(cryptoKeyPair),
        Crypto.exportPrivateKey(cryptoKeyPair),
    ]);

    const [base64Public, base64Private] = await Promise.all([
        Crypto.convertArrayToBase64(exportedPublic),
        Crypto.convertArrayToBase64(exportedPrivate),
    ]);

    const newKeyPair = {
        publicKey: base64Public,
        privateKey: base64Private,
    };

    State.keyPair = newKeyPair;

    return newKeyPair;
})();

