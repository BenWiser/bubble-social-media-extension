
const createUser = async (keyPair) => {
    const userId = await requestText('users', 'PUT', {
        publicKey: keyPair.publicKey,
    });

    return userId;
};

// Try preload the user before they open their sidebar menu
(async () => {
    const userId = await State.userId;
    if (userId === undefined) {
        // The authentication key stays local but hold off generating it to make sure this user has it
        await initialiseAuthenticationKey;
        const keyPair = await initialiseKeyPair;
        State.userId = await createUser(keyPair);
    }
    setBubbleIdentityFileDownload();
})();

const getAuthenticationKey = async () => {
    const authenticationKey = await State.authenticationKey;
    const authenticationArray = Crypto.convertBase64toArray(authenticationKey);
    const importedAuthentication = await Crypto.importAuthenticationKey(authenticationArray);
    return importedAuthentication;
};

document.addEventListener('ChangeNameEvent', ({ detail: { name } }) => {
    State.name = name;
});

document.addEventListener('FollowEvent', async ({ detail: { bubbleIdentity } }) => {
    const name = await State.name;
    const [bubbleId, authenticationId, publicKey] = bubbleIdentity.split(/\n/);
    const importedPublicKey = await Crypto.importPublicKey(
        Crypto.convertBase64toArray(publicKey)
    );
    const encryptedName = await Crypto.convertArrayToBase64(
        new Uint8Array(
            await Crypto.encryptMessage(importedPublicKey, name)
        )
    );
    State.addAuthenticationId({
        bubbleId,
        authenticationId,
    });
    const userId = await State.userId;
    await requestText(`users/${bubbleId}/followers`, 'POST', {
        followerId: userId,
        name: encryptedName,
    });
});

document.addEventListener('CommentEvent', async ({ detail: { comment } }) => {
    const [name, userId, followers, authenticationKey] = await Promise.all([
        State.name,
        State.userId,
        State.followers,
        getAuthenticationKey(),
    ]);

    // Create a new key for this comment
    const iv = crypto.getRandomValues(new Uint8Array(12));
    const symetricKey = await Crypto.createSymetricKey();
    const exportedSymetricKey = await Crypto.exportSymetricKey(symetricKey, iv);

    const getEncryptedBase64 = async (data) => Crypto.convertArrayToBase64(
        new Uint8Array(
            await Crypto.encryptSymetricMessage(symetricKey, data, iv)
        ),
    );

    // Encrypt this comment
    const [encryptedName, url, pageTitle, message] = await Promise.all([
        getEncryptedBase64(name),
        getEncryptedBase64(location.href),
        getEncryptedBase64(document.title),
        getEncryptedBase64(comment),
    ]);

    const commentId = await requestText('comments', 'PUT', {
        url,
        pageTitle,
        userId,
        message,
        name: encryptedName,
    });

    const encryptedComments = await Promise.all(followers
        .filter(({ allow }) => Boolean(allow))
        .map(async ({ followerId, follower: { publicKey } }) => {
        const importedPublicKey = await Crypto.importPublicKey(
            Crypto.convertBase64toArray(publicKey)
        );

        const key = await Crypto.convertArrayToBase64(
            new Uint8Array(
                await Crypto.encryptMessage(importedPublicKey, JSON.stringify(exportedSymetricKey))
            )
        );

        const proof = Crypto.convertArrayToBase64(new Uint8Array(await Crypto.sign(authenticationKey, key)));

        return {
            key,
            proof,
            commentId,
            receiverId: followerId,
            senderId: userId,
        };
    }));

    // Add the comment locally
    await State.addComment({
        userId,
        name: name,
        message: comment,
        url: location.href,
        pageTitle: document.title,
    });

    requestText(`comments/users`, 'PUT', {
        userComments: encryptedComments,
    });
    sync();
});

document.addEventListener('AllowFollowerEvent', async ({ detail: { followerId } }) => {
    const followers = await State.followers;
    for (const follower of followers) {
        if (follower.followerId === followerId) {
            follower.allow = true;
        }
    }
    State.followers = followers;
    sync();
});

document.addEventListener('DenyFollowerEvent', async ({ detail: { followerId } }) => {
    const followers = await State.followers;
    for (const follower of followers) {
        if (follower.followerId === followerId) {
            follower.deny = true;
        }
    }
    State.followers = followers;
    sync();
});

// Look for new comments
const lookForComments = async () => {
    const [userId, lastChecked, keyPair, comments] = await Promise.all([State.userId, State.lastChecked, State.keyPair, State.comments]);

    if (userId !== undefined) {
        const date = lastChecked ? lastChecked : '';
        
        const encryptedComments = await requestJson(`users/${userId}/comments?date=${date}`);
        
        const privateKey = await Crypto.importPrivateKey(Crypto.convertBase64toArray(keyPair.privateKey));
        
        const authenticationIds = await State.authenticationIds;

        let newComments = await Promise.all(encryptedComments.map(async ({
            senderId,
            proof,
            key,
            comment: {
                message,
                name,
                createdAt,
                url,
                pageTitle,
                userId,
            },
        }) => {
            const authenticationDetails = authenticationIds.find(({ bubbleId }) => senderId === bubbleId);
            if (!authenticationDetails) {
                return null;
            }

            const proofArray = Crypto.convertBase64toArray(proof);
            const authenticationArray = Crypto.convertBase64toArray(authenticationDetails.authenticationId);
            const authenticationKey = await Crypto.importAuthenticationKey(authenticationArray);

            const isValid = await Crypto.verify(authenticationKey, proofArray, key);

            if (!isValid) {
                return null;
            }

            // Retrieve the symetric key for this comment
            const decryptSymetricKey = async (data) => {
                const dataArray = Crypto.convertBase64toArray(data);
                const decrypted = await Crypto.decryptMessage(privateKey, dataArray);
                return decrypted;
            };

            const messageEncryptionKey = JSON.parse(await decryptSymetricKey(key));
            const importedSymetricKey = await Crypto.importSymetricKey(messageEncryptionKey);

            const decryptMessageContent = async (data) => Crypto.decryptSymetricMessage(
                importedSymetricKey.key,
                Crypto.convertBase64toArray(data),
                importedSymetricKey.iv,
            );
            
            const [decryptedMessage, decryptedName, decryptedUrl, decryptedPageTitle] = await Promise.all([
                decryptMessageContent(message),
                decryptMessageContent(name),
                decryptMessageContent(url),
                decryptMessageContent(pageTitle),
            ]);

            return {
                userId,
                createdAt,
                url: decryptedUrl,
                pageTitle: decryptedPageTitle,
                name: decryptedName,
                key: messageEncryptionKey,
                message: decryptedMessage,
            };
        }));

        newComments = newComments
            .filter(comment => comment !== null);
        
        State.comments = comments ? comments.concat(newComments) : newComments;
        State.lastChecked = new Date().toISOString();

        updateComments();
    }
};

const updateFollowerRequests = async () => {
    const followerList = sideBar.querySelector('.followerRequests ul');
    followerList.innerHTML = "";
    const [followers, keyPair] = await Promise.all([
        State.followers,
        State.keyPair,
    ]);
    const privateKey = await Crypto.importPrivateKey(
        Crypto.convertBase64toArray(keyPair.privateKey)
    );

    for await (const follower of followers) {
        if (follower.deny) {
            continue;
        }

        const name = Crypto.convertBase64toArray(follower.name);
        const decryptedName = await Crypto.decryptMessage(privateKey, name);

        const followerListItem = document.createElement('li');
        followerListItem.innerHTML = decryptedName;

        if (!follower.allow) {
            followerListItem.innerHTML += `
                <button class="allowFollowerButton">Allow</button>
                <button class="denyFollowerButton">Deny</button>`;

            followerListItem.querySelector('.allowFollowerButton').addEventListener("click", (event) => {
                const allowerFollowerEvent = new CustomEvent('AllowFollowerEvent', {
                    detail: {
                        followerId: follower.followerId,
                    },
                });
                document.dispatchEvent(allowerFollowerEvent);
            });

            followerListItem.querySelector('.denyFollowerButton').addEventListener("click", (event) => {
                const denyFollowerEvent = new CustomEvent('DenyFollowerEvent', {
                    detail: {
                        followerId: follower.followerId,
                    },
                });
                document.dispatchEvent(denyFollowerEvent);
            });
        }

        followerList.appendChild(followerListItem);
    }
};

const lookForFollowers = async () => {
    const userId = await State.userId;
    const [requestingFollowers, followers] = await Promise.all([
        requestJson(`users/${userId}/followers`),
        State.followers,
    ]);

    const followerIds = followers.map(({ followerId }) => followerId);
    const newFollowers = requestingFollowers.filter((follower) => followerIds.indexOf(follower.followerId) === -1);

    State.followers = [...followers, ...newFollowers];
    updateFollowerRequests();
};

const sync = () => {
    lookForComments();
    lookForFollowers();
};