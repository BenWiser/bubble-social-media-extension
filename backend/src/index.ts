import * as express from 'express';
import * as expressWs from 'express-ws';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as moment from 'moment';
import { Op } from 'sequelize';
import connection from './models/connection';
import { Comments, Users, Followers, UserComments } from './models';

connection.sync().then(() => {
    const Server: any = express();
    expressWs(Server);
    Server.use(bodyParser.urlencoded({ extended: false }));
    Server.use(bodyParser.json());
    Server.use(cors());

    interface SocketRequest {
        type: String;
        message: any;
    }

    Server.ws('/bubble', (ws, req) => {
        ws.on('message', async (msg: string) => {
            const socketResponse: SocketRequest = JSON.parse(msg);
            switch(socketResponse.type) {
                case "join": {
                    const publicKey = socketResponse.message.publicKey;

                    const user = await Users.create({
                        publicKey
                    });

                    ws.send(JSON.stringify({
                        type: 'userJoined',
                        userId: user.id,
                    }));

                    break;
                }
            }
        });
    });

    Server.get('/users/:userId/followers', async ({ params: { userId } }, res) => {
        const followers = await Followers.findAll({
            where: {
                userId,
            },
            attributes: ['followerId', "name"],
            include: [{
                model: Users,
                as: 'follower',
                attributes: ['publicKey']
            }],
        });
        res.send(followers);
    });

    Server.post('/users/:userId/followers', async ({ body: { followerId, name }, params: { userId } }) => {
        await Followers.create({
            name,
            userId,
            followerId,
        })
    });

    Server.put('/users', async ({ body: { publicKey } }, res) => {
        const user = await Users.create({
            publicKey,
        });
        res.send(user.id);
    });

    Server.get("/users/:userId/publicKey", async ({ params: { userId } }, res) => {
        const user = await Users.findByPk(userId);
        res.send(user.publicKey)
    });

    Server.get("/users/:userId/comments", async ({ params: { userId }, query: { date } }, res) => {
        const where: any = {
            receiverId: userId,
        };
        if (date) {
            where.createdAt = {
                [Op.gte]: moment(date).format("YYYY-MM-DD HH:mm:ss"),
            };
        }

        const userComments = await UserComments.findAll({
            where,
            include: [Comments],
        });

        res.send(userComments);
    });

    Server.put("/comments", async ({ body: { url, pageTitle, userId, message, name } }, res) => {
        const comment = await Comments.create({
            url,
            userId,
            pageTitle,
            message,
            name,
        });
        res.send(comment.id);
    });

    Server.put("/comments/users", async ({ body: { userComments } }) => {
        await UserComments.bulkCreate(userComments);
    });

    const PORT: number = 9000;
    Server.listen(PORT, () => {
        console.log(`Listening on port ${PORT}`);
    });
});