import { Sequelize } from 'sequelize';

const connection = new Sequelize({
    database: 'bubble',
    dialect: 'sqlite',
    storage: 'db.sq3'
});

export default connection;