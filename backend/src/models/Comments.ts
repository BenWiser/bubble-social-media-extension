import { TEXT, Model, UUID, UUIDV4 } from 'sequelize';
import connection from './connection';

class Comments extends Model {}

Comments.init({
    id: {
        type: UUID,
        defaultValue: UUIDV4,
        primaryKey: true,
    },
    name: {
        type: TEXT,
    },
    pageTitle: {
        type: TEXT,
    },
    url: {
        type: TEXT
    },
    message: {
        type: TEXT
    }
}, { sequelize: connection, modelName: 'comments' });

export default Comments;