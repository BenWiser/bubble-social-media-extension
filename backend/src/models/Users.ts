import { UUIDV4, Model, TEXT, UUID } from 'sequelize';
import connection from './connection';

class Users extends Model {}

Users.init({
    id: {
        type: UUID,
        defaultValue: UUIDV4,
        primaryKey: true,
    },
    publicKey: {
        type: TEXT,
    },
}, { sequelize: connection, modelName: 'users' });

export default Users;