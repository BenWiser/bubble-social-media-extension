import { Model, TEXT } from 'sequelize';
import connection from './connection';
import Users from './Users';

class Followers extends Model {}

Followers.init({
    name: {
        type: TEXT,
    },
}, { sequelize: connection, modelName: 'followers' });

Followers.belongsTo(Users, { as: 'user' });
Followers.belongsTo(Users, { as: 'follower' });

export default Followers;