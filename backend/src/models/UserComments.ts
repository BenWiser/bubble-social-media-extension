import { TEXT, Model, UUID } from 'sequelize';
import connection from './connection';
import Users from './Users';
import Comments from './Comments';

/**
 * This model represents all the comments and the users attached to them
 */
class UserComments extends Model {}

UserComments.init({
    key: {
        type: TEXT
    },
    proof: {
        type: TEXT,
    },
    receiverId: {
        type: UUID,
    },
    senderId: {
        type: UUID,
    }
}, { sequelize: connection, modelName: 'userComments' });

UserComments.belongsTo(Comments)

Users.hasMany(UserComments, { as: 'receiver', foreignKey: 'receiverId' });
Users.hasMany(UserComments, { as: 'sender', foreignKey: 'senderId' });

Comments.belongsTo(Users);

export default UserComments;