# Immune - Social Media in your browser

Immune is a social media app that sits right in your browser.

Invert the relationship of social media by interacting from websites instead of the other way around.

### Disclaimer

Please don't judge me too hard on some of the code quality.

I shot through this in a weekend.

### Installation

You can self host the server by running `yarn start`
in the background directory and pointing your extension
to your server.

You can also then self install the extension as you would
with any other chrome app.

### References

`trust.png` was created by https://www.flaticon.com/authors/freepik

`globe.png` was created by https://www.flaticon.com/authors/icongeek26

`search.png` was created by https://www.flaticon.com/authors/freepik

`comment.png` was created by https://www.flaticon.com/authors/freepik

`find.png` was created byy https://www.flaticon.com/authors/those-icons

`share` was created by https://www.flaticon.com/authors/pixel-perfect