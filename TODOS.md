[x] Add feed
[x] Add export file
[ ] Add tests
  [ ] Add encryption tests
[ ] Add build chain - possibly roll up
[ ] Add database migration
[x] Improve styling
[ ] Add syncing through 3rd party services
[ ] Reload on actions
[ ] Add reply chains
    - This can be done by sharing the encryption key with the replys
[ ] Add daisy chaining
[x] Allow/Deny followers
[ ] Prevent following yourself or others more than once